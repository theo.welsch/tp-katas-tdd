﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.FizzBuzz
{
    public class BuzzRule: IRules
    {
        private static readonly int NUMBER_BUZZ_MULTIPLE = 5;
        public bool Matches(int number)
        {
            return number % NUMBER_BUZZ_MULTIPLE == 0;
        }

        public string GetReplacement()
        {
            return "Buzz";
        }
    }
}
