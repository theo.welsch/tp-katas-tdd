﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.FizzBuzz
{
    public class FizzBuzzRule: IRules
    {
        private static readonly FizzRule FizzRule = new FizzRule();
        private static readonly BuzzRule BuzzRule = new BuzzRule();
        public bool Matches(int number)
        {
            return FizzRule.Matches(number) && BuzzRule.Matches(number);
        }

        public string GetReplacement()
        {
            return FizzRule.GetReplacement() + BuzzRule.GetReplacement();
        }
    }
}
