﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.FizzBuzz
{
    public interface IRules
    {
        public abstract bool Matches(int number);
        public abstract string GetReplacement();
    }
}
