﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.FizzBuzz
{
    public class FizzBuzzValue
    {
        public int Number { get; }

        public FizzBuzzValue(int number)
        {
            if(number < 15 || number > 150)
            {
                throw new ArgumentOutOfRangeException("Le nombre choisi doit etre compris entre 15 et 150");
            }
            this.Number = number;
        }
    }
}
