﻿using Xunit;
using Katas.Tennis;
using System;

namespace TestKatas.TestTennis
{
    public class TestTennis
    {
        [Fact]
        public void ErrorFizzBuzzNumber()
        {
            ArgumentOutOfRangeException exception = Assert.Throws<ArgumentOutOfRangeException>(() => new TennisScore(-1));
            Assert.Equal("Le nombre choisi doit etre un nombre entier positif", exception.ParamName);
        }

        [Fact]
        public void TestLoveAll()
        {
            Assert.Equal("Love-All", Tennis.GetScore(new TennisScore(0), new TennisScore(0)));
        }

        [Fact]
        public void TestFifteenAll()
        {
            Assert.Equal("Fifteen-All", Tennis.GetScore(new TennisScore(1), new TennisScore(1)));
        }

        [Fact]
        public void TestThirtyAll()
        {
            Assert.Equal("Thirty-All", Tennis.GetScore(new TennisScore(2), new TennisScore(2)));
        }

        [Theory]
        [InlineData(3)]
        [InlineData(4)]
        public void TestDeuce(int points)
        {
            Assert.Equal("Deuce", Tennis.GetScore(new TennisScore(points), new TennisScore(points)));
        }

        [Theory]
        [InlineData("Love-Forty", 0, 3)]
        [InlineData("Fifteen-Thirty", 1, 2)]
        [InlineData("Thirty-Fifteen", 2, 1)]
        [InlineData("Forty-Love", 3, 0)]
        public void TestClassicScore(string expectedScore, int player1Points, int player2Points)
        {
            Assert.Equal(expectedScore, Tennis.GetScore(new TennisScore(player1Points), new TennisScore(player2Points)));
        }

        [Theory]
        [InlineData("Advantage Player1", 5, 4)]
        [InlineData("Advantage Player2", 6, 7)]
        public void TestAdvantageScore(string expectedScore, int player1Points, int player2Points)
        {
            Assert.Equal(expectedScore, Tennis.GetScore(new TennisScore(player1Points), new TennisScore(player2Points)));
        }

        [Theory]
        [InlineData("Win Player1", 4, 0)]
        [InlineData("Win Player2", 4, 6)]
        public void TestWinScore(string expectedScore, int player1Points, int player2Points)
        {
            Assert.Equal(expectedScore, Tennis.GetScore(new TennisScore(player1Points), new TennisScore(player2Points)));
        }

    }
}
