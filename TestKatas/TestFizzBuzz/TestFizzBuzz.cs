﻿using Xunit;
using Katas.FizzBuzz;
using System;

namespace TestKatas.TestFizzBuzz
{
    public class TestFizzBuzz
    {
        [Fact]
        public void Return1If1()
        {
            Assert.Equal("1", FizzBuzz.Generate(1));
        }

        [Fact]
        public void Return12If2()
        {
            Assert.Equal("12", FizzBuzz.Generate(2));
        }

        [Fact]
        public void Return12FizzIf3()
        {
            Assert.Equal("12Fizz", FizzBuzz.Generate(3));
        }

        [Fact]
        public void Return12Fizz4BuzzIf5()
        {
            Assert.Equal("12Fizz4Buzz", FizzBuzz.Generate(5));
        }

        [Theory]
        [InlineData(14)]
        [InlineData(151)]
        public void ErrorFizzBuzzNumber(int number)
        {
            ArgumentOutOfRangeException exception = Assert.Throws<ArgumentOutOfRangeException>(() => new FizzBuzzValue(number));
            Assert.Equal("Le nombre choisi doit etre compris entre 15 et 150", exception.ParamName);
        }

        [Fact]
        public void ValidFizzBuzzNumber15()
        {
            Assert.Equal("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", FizzBuzz.Generate(new FizzBuzzValue(15)));
        }
    }
}
